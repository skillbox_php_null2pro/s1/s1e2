<?php

//check request
if(empty($_GET['file_name']))
{
	http_response_code(404);
	die();
}

//send file
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename=' . urlencode($_GET['file_name'].'.xls'));